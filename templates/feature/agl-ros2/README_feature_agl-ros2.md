---
description: Feature agl-ros2
authors: Shankho Boron Ghosh <shankhoghosh123@gmail.com>
---

### Feature agl-ros2

Enables building of meta-ros2-foxy layer with additional support for YDLIDAR drivers.